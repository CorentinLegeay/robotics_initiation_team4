# /usr/bin/python3
# coding : utf8

#  importation des librairies
import pypot.dynamixel
from pypot import *
import time
import io
import numpy as np
import math
import os
import keyboard
import sys

import traceback
# libraires locales
import walk
import robot
import function


def final():
    # partie final à executer pour terminer le programme
    robot.all_m(100, 0, 120)
    time.sleep(0.05)
    robot.disable_motor()  # si l'erreur n'est pas résolu stopper la force des moteurs
    print("-------")
    print("Finish, please wait 2 sec")
    time.sleep(2)
    SystemExit  # permet de sortir du programme

#   programme principal
if __name__ == '__main__':
    robot.init()
    #robot.zero_position()
    time.sleep(0.5)
    robot.all_m(100, 0, 120)
    time.sleep(0.5)
    print("ok")
    time.sleep(0.5)

    while True:

### DEPLACEMENT ###
        if keyboard.is_pressed("z"):
            while keyboard.is_pressed("z"):
                #Déplacement vers l'avant
                walk.avant()
                print("ok")
                pass
            print("fin avancement")

        if keyboard.is_pressed("s"):
            while keyboard.is_pressed("s"):
                #Déplacement vers l'arrière
                walk.arriere()
                print("ok")
                pass
            print("fin avancement")

        if keyboard.is_pressed("q"):
            while keyboard.is_pressed("q"):
                #Translation vers la gauche
                walk.cote_g()
                print("ok")
                pass
            print("fin avancement")

        if keyboard.is_pressed("d"):
            while keyboard.is_pressed("d"):
                #Translation vers la droite
                walk.cote_d()
                print("ok")
                pass
            print("fin avancement")

        if keyboard.is_pressed("a"):
            while keyboard.is_pressed("a"):
                #Rotation sur lui même vers la droite
                walk.tourne_d()
                print("ok")
                pass
            print("fin avancement")

        if keyboard.is_pressed("e"):
            while keyboard.is_pressed("e"):
                #Rotation sur lui même vers la gauche
                walk.tourne_g()
                print("ok")
                pass
            print("fin avancement")           

### REGLAGE HAUTEUR ###
        if keyboard.is_pressed("4"):
            while keyboard.is_pressed("4"):
                #incrémenter la hauteur de l'hexapod
                walk.z -= 1
                robot.all_m(walk.off_x, 0, walk.z)
                time.sleep(0.01)
                print(walk.z)
                print("ok")
                pass
            print("fin avancement")
        if keyboard.is_pressed("7"):
            while keyboard.is_pressed("7"):
                #décrementer la hauteur de l'hexapod
                walk.z += 1
                robot.all_m(walk.off_x, 0, walk.z)
                time.sleep(0.01)
                print(walk.z)
                print("ok")
                pass
            print("fin avancement")

### REGLAGE LONGUEUR DES PATTES EN X ###
        if keyboard.is_pressed("5"):
            while keyboard.is_pressed("5"):
                #incrémenter la longueur des pattes de l'hexapod
                walk.off_x -= 1
                robot.all_m(walk.off_x, 0, walk.z)
                time.sleep(0.05)
                print(walk.off_x)
                print("ok")
                pass
            print("fin avancement")

        if keyboard.is_pressed("8"):
            while keyboard.is_pressed("8"):
                #décrementer la longueur des pattes de l'hexapod
                walk.off_x += 1
                robot.all_m(walk.off_x, 0, walk.z)
                time.sleep(0.05)
                print(walk.off_x)
                print("ok")
                pass
            print("fin avancement")
            
### REGLAGE DE L'ANGLE THETA ### 
        if keyboard.is_pressed("+"):
            while keyboard.is_pressed("+"):
                #incrémenter la fonction lever chassis
                robot.deltaTheta += 1
                robot.all_m(walk.off_x, 0, walk.z)
                time.sleep(0.05)
                print(robot.deltaTheta)
                #print("ok")
                pass
            print("fin avancement")

        if keyboard.is_pressed("-"):
            while keyboard.is_pressed("-"):
                #incrémenter la fonction baisser chassis
                robot.deltaTheta -= 1
                robot.all_m(walk.off_x, 0, walk.z)
                time.sleep(0.05)
                print(robot.deltaTheta)
                #print("ok")
                pass
            print("fin avancement")

        if keyboard.is_pressed("l"):
            while keyboard.is_pressed("l"):
                #incrémenter la fonction baisser chassis
                walk.attack()
                print("ok")
                pass
            print("fin avancement")
        
        if keyboard.is_pressed("f"): #touche f stop le programme donc une fois inclus voir si il faut l'enlever
            print("\nFin du programme passage à la boucle final")
            final()
            
            break
        else:
            pass
        
    
    #move your boby
    taille_cercle = 50
    while 1:
        for i in range(0, 360):
            walk.move_boby(i, taille_cercle)
            time.sleep(0.01)
