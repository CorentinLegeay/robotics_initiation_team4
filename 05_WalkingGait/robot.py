# /usr/bin/python3
# coding : utf8

import pypot.dynamixel
import time
from pypot import *
import io
import function
import numpy as np
import math
import walk
import traceback


# Récupération du port
ports = pypot.dynamixel.get_available_ports()
# S'il n'y a pas de port disponible alors on retourne une erreur
if not ports:
    raise IOError('no port found!')
# Si un port est disponible, on se connecte
print('ports found', ports)
print('connecting on the first available port:', ports[0])
dxl_io = pypot.dynamixel.DxlIO(ports[0])

deltaTheta = 0

def init():
    """'
    Fonction d'initialisation des moteurs
    """
    # Position initiale des moteurs
    dxl_io.set_joint_mode({11, 12, 13, 21, 22, 23, 31, 32, 33, 41, 42, 43, 51, 52, 53, 61, 62, 63})
    time.sleep(0.5)
    # Initialisation des angles limites des moteurs
    dxl_io.set_angle_limit({11: (-100, 100), 12: (-100, 100), 13: (-110, 70)})
    dxl_io.set_angle_limit({21: (-100, 100), 22: (-100, 100), 23: (-70, 110)})
    dxl_io.set_angle_limit({31: (-100, 100), 32: (-100, 100), 33: (-70, 110)})
    dxl_io.set_angle_limit({41: (-100, 100), 42: (-100, 100), 43: (-70, 110)})
    dxl_io.set_angle_limit({51: (-100, 100), 52: (-100, 100), 53: (-110, 70)})
    dxl_io.set_angle_limit({61: (-100, 100), 62: (-100, 100), 63: (-110, 70)})
    time.sleep(0.5)


def disable_motor():
    """
    Fonction de desactivation des moteurs
    """
    # Désactivation des moteurs
    dxl_io.disable_torque({11, 12, 13, 21, 22, 23, 31, 32, 33, 41, 42, 43, 51, 52, 53, 61, 62, 63})


def zero_position():
    delay = 1
    dxl_io.set_goal_position({11: 0, 12: 0,  13: 0})
    time.sleep(delay)
    dxl_io.set_goal_position({21: 0, 22: 0, 23: 0})
    time.sleep(delay)
    dxl_io.set_goal_position({31: 0, 32: 0, 33: 0})
    time.sleep(delay)
    dxl_io.set_goal_position({41: 0, 42: 0, 43: 0})
    time.sleep(delay)
    dxl_io.set_goal_position({51: 0, 52: 0, 53: 0})
    time.sleep(delay)
    dxl_io.set_goal_position({61: 0, 62: 0, 63: 0})
    time.sleep(delay)

### FONCTION JAMBE ###

def jambe1(x, y, z):
    # On aligne l'origine de la patte avec l'origine du robot
    theta = -45 + deltaTheta
    theta = math.radians(theta)
    x = x * np.cos(theta) + y * np.sin(theta)
    y = -x * np.sin(theta) + y * np.cos(theta)
    # On appelle la fonction leg_ik()
    theta1, theta2, theta3 = function.leg_ik(x, y, z)
    print("JAMBE 1",theta1, theta2, theta3)
    # On fait bouger la patte
    dxl_io.set_goal_position({11: -theta1, 12: theta2, 13: theta3})


def jambe2(x, y, z):
    # On aligne l'origine de la patte avec l'origine du robot
    theta = -45 - deltaTheta
    theta = math.radians(theta)
    x = x * np.cos(theta) + y * np.sin(theta)
    y = -x * np.sin(theta) + y * np.cos(theta)
    # On appelle la fonction leg_ik()
    theta1, theta2, theta3 = function.leg_ik(x, y, z)
    print("JAMBE 2",theta1, theta2, theta3)
    # On fait bouger la patte
    dxl_io.set_goal_position({21: theta1, 22: -theta2, 23: -theta3})


def jambe3(x, y, z):
    # On aligne l'origine de la patte avec l'origine du robot
    theta = 0 - deltaTheta
    theta = math.radians(theta)
    x = x * np.cos(theta) + y * np.sin(theta)
    y = -x * np.sin(theta) + y * np.cos(theta)
    # On appelle la fonction leg_ik()
    theta1, theta2, theta3 = function.leg_ik(x, y, z)
    print("JAMBE 3",theta1, theta2, theta3)
    # On fait bouger la patte
    dxl_io.set_goal_position({31: theta1, 32: -theta2, 33: -theta3})


def jambe4(x, y, z):
    # On aligne l'origine de la patte avec l'origine du robot
    theta = 45 - deltaTheta
    theta = math.radians(theta)
    x = x * np.cos(theta) + y * np.sin(theta)
    y = -x * np.sin(theta) + y * np.cos(theta)
    # On appelle la fonction leg_ik()
    theta1, theta2, theta3 = function.leg_ik(x, y, z)
    print("JAMBE 4",theta1, theta2, theta3)
    # On fait bouger la patte
    dxl_io.set_goal_position({41: theta1, 42: -theta2, 43: -theta3})


def jambe5(x, y, z):
    # On aligne l'origine de la patte avec l'origine du robot
    theta = 45 + deltaTheta
    theta = math.radians(theta)
    x = x * np.cos(theta) + y * np.sin(theta)
    y = -x * np.sin(theta) + y * np.cos(theta)
    # On appelle la fonction leg_ik()
    theta1, theta2, theta3 = function.leg_ik(x, y, z)
    print("JAMBE 5",theta1, theta2, theta3)
    # On fait bouger la patte
    dxl_io.set_goal_position({51: -theta1, 52: theta2, 53: theta3})

def jambe6(x, y, z):
    # On aligne l'origine de la patte avec l'origine du robot
    theta = 0 + deltaTheta
    theta = math.radians(theta)
    x = x * np.cos(theta) + y * np.sin(theta)
    y = -x * np.sin(theta) + y * np.cos(theta)
    # On appelle la fonction leg_ik()
    theta1, theta2, theta3 = function.leg_ik(x, y, z)
    print("JAMBE 6",theta1, theta2, theta3)
    # On fait bouger la patte
    dxl_io.set_goal_position({61: -theta1, 62: theta2, 63: theta3})


# definition des triangles pour le deplacement
def triangle_A(x, y, z):
    jambe1(x, y, z)
    jambe3(x, y, z)
    jambe5(x, y, z)


def triangle_B(x, y, z):
    jambe2(x, y, z)
    jambe4(x, y, z)
    jambe6(x, y, z)


def all_m(x, y, z):
    triangle_A(x, y, z)
    triangle_B(x, y, z)