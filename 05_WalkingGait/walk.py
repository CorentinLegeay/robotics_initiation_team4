#!/usr/bin/python3
import pypot
import time
import function
import numpy as np
import math as m
import robot

z = 140 #hauteur de l'hexapod
off_x = 120

def avant():
    #definition des constantes
    x = off_x #off set en x
    
    ty = 75  # deplacement total par pattes
    hz = 30  # hauteur du levé de patte
    var = m.radians(180 / ty)
    for i in range(0, ty):
        # deplacement triangle A
        y = (ty / 2) - i # deplacement sur y de ty/2 a -ty/2
        bz = z - (hz * np.sin(i * var)) #levé de z en demi cercle de rayon hz
        robot.triangle_A(x, y, bz) #envoie de la donnée au triangle A

        # deplacment triangle B
        y = -(ty / 2) + i
        robot.triangle_B(x, y, z)
        time.sleep(0.005)
        
    # deplacement en x ade -ty a ty
    for i in range(0, ty):
        # deplacement triangle A
        y = -(ty / 2) + i
        robot.triangle_A(x, y, z)

        # deplacment triangle B
        y = (ty / 2) - i
        bz = z - (hz * np.sin(i * var))
        robot.triangle_B(x, y, bz)
        time.sleep(0.005)

def arriere():
    #definition des constantes
    x = off_x #off set en x
    #z = 140 #hauteur de l'hexapod
    ty = 75  # deplacement total par pattes
    hz = 30  # hauteur du levé de patte
    var = m.radians(180 / ty)
    # deplacement en x de ty a -ty
    for i in range(0, ty):
        # deplacement triangle A
        y = (ty / 2) - i
        robot.triangle_A(x, y, z)

        # deplacement triangle B
        y = -(ty / 2) + i
        bz = z - (hz * np.sin(i * var))
        robot.triangle_B(x, y, bz)
        time.sleep(0.005)

    for i in range(0, ty):
        y = -(ty / 2) + i
        bz = z - (hz * np.sin(i * var))
        robot.triangle_A(x, y, bz)

        # deplacement triangle B
        y = (ty / 2) - i
        robot.triangle_B(x, y, z)
        time.sleep(0.005)

def cote_g():
    #definition des constantes
    y = 0 #off set en y
    #z = 140 #hauteur de l'hexapod
    tx = 80 #deplacement total par pattes
    hz = 30 #haute du levé de patte
    var = m.radians(180 / tx)

    # deplacement sur x de tx a -tx
    for i in range(0, tx):
        # deplacement triangle A
        x = ((off_x)+(tx/2))-i
        robot.jambe1(x ,y ,z)
        robot.jambe5(x ,y ,z)
        x = ((off_x)-(tx/2))+i
        robot.jambe3(x ,y ,z)
        
        # deplacment triangle B
        x = ((off_x)-(tx/2))+i
        bz = z - (hz * np.sin(i * var))
        robot.jambe6(x, y, bz)
        x = ((off_x)+(tx/2))-i
        robot.jambe2(x, y, bz)
        robot.jambe4(x, y, bz)
        time.sleep(0.005)
    
    # deplacement en x de -tx a tx
    for i in range(0, tx):
        # deplacement triangle A
        x = ((off_x)-(tx/2))+i
        bz = z - (hz * np.sin(i * var))
        robot.jambe1(x ,y ,bz)
        robot.jambe5(x ,y ,bz)
        x = ((off_x)+(tx/2))-i
        robot.jambe3(x ,y ,bz)

        # deplacment triangle B
        x = ((off_x)+(tx/2))-i
        robot.jambe6(x ,y ,z)
        x = ((off_x)-(tx/2))+i
        robot.jambe2(x, y, z)
        robot.jambe4(x, y, z)
        time.sleep(0.005)

def cote_d():
    #definition des constantes
    y = 0 #off set en y
    #z = 140 #hauteur de l'hexapod
    tx = 80 #deplacement total par pattes
    hz = 30 #haute du levé de patte
    var = m.radians(180 / tx)
    # deplacement sur x de tx a -tx
    
    for i in range(0, tx):
        # deplacement triangle A
        x = ((off_x)-(tx/2))+i
        robot.jambe1(x ,y ,z)
        robot.jambe5(x ,y ,z)
        x = ((off_x)+(tx/2))-i
        robot.jambe3(x ,y ,z)
        
        # deplacment triangle B
        x = ((off_x)+(tx/2))-i
        bz = z - (hz * np.sin(i * var))
        robot.jambe6(x ,y ,bz)
        x = (off_x-(tx/2))+i
        robot.jambe2(x, y, bz)
        robot.jambe4(x, y, bz)
        time.sleep(0.005)
    
    # deplacement en x de -tx a tx
    for i in range(0, tx):
        # deplacement triangle A
        x = (off_x+(tx/2))-i
        bz = z - (hz * np.sin(i * var))
        robot.jambe1(x ,y ,bz)
        robot.jambe5(x ,y ,bz)
        x = (off_x-(tx/2))+i
        robot.jambe3(x ,y ,bz)

        # deplacment triangle B
        x = (off_x-(tx/2))+i
        robot.jambe6(x, y, z)
        x = (off_x+(tx/2))-i
        robot.jambe2(x, y, z)
        robot.jambe4(x, y, z)
        time.sleep(0.005)
            
def tourne_g():
    correction1 = 13.4*10
    correction2 = 10*10
    ty = 50
    x = off_x #off set en x
    #z = 140 #hauteur de l'hexapod
    hz = 30 #haute du levé de patte
    var = m.radians(180 / ty)

    # deplacement sur x de ty a -ty
    for i in range(0, ty):
        # deplacement triangle A de 
        y = (ty / 2) - i #evolution en y de ty/2 a -ty/2
        bz = z - (hz * np.sin(i * var)) #demi cercle de rayon hz en z
        robot.jambe1(x, y, bz)
        robot.jambe5(x, y, bz)
        y = -(ty / 2) + i
        robot.jambe3(x, y, bz)


        # deplacment triangle B
        y = (ty / 2) - i
        print((x+ correction1)/(ty/2))
        bx = (x + correction1) * np.sin(20 - (i*20))
        robot.jambe2(x, y, z)
        robot.jambe4(x, y, z)
        y = -(ty / 2) + i
        bx = (x + correction2) * np.sin(i * var)
        robot.jambe6(x, y, z)
        time.sleep(0.005)

    # deplacement en x de -ty a ty
    for i in range(0, ty):
        # deplacement triangle A
        y = -(ty / 2) + i
        bx = (x + correction1) * np.sin(20 - (i*20))
        robot.jambe1(x, y, z)
        robot.jambe5(x, y, z)
        y = (ty / 2) - i
        bx = (x + correction2) * np.sin(i * var)
        robot.jambe3(x, y, z)

        # deplacment triangle B
        y = -(ty / 2) + i
        bz = z - (hz * np.sin(i * var))
        robot.jambe2(x, y, bz)
        robot.jambe4(x, y, bz)
        y = (ty / 2) - i
        robot.jambe6(x, y, bz)
        time.sleep(0.005)

def tourne_d ():
    correction1 = 13.4*10
    correction2 = 10*10
    ty = 50
    x = off_x #off set en x
    #z = 140 #hauteur de l'hexapod
    hz = 30 #haute du levé de patte
    var = m.radians(180 / ty)

    for i in range(0, ty):
        # deplacement triangle A de 
        y = (ty / 2) - i #evolution en y de ty/2 a -ty/2
        bx = (x + correction1) * np.sin(20 - (i*20))
        robot.jambe1(x, y, z)
        robot.jambe5(x, y, z)
        y = -(ty / 2) + i
        bx = (x + correction2) * np.sin(i * var)
        robot.jambe3(x, y, z)


        # deplacment triangle B
        y = (ty / 2) - i
        bz = z - (hz * np.sin(i * var))
        robot.jambe2(x, y, bz)
        robot.jambe4(x, y, bz)
        y = -(ty / 2) + i
        robot.jambe6(x, y, bz)
        time.sleep(0.005)

    # deplacement en x de -ty a ty
    for i in range(0, ty):
        # deplacement triangle A
        y = -(ty / 2) + i
        bz = z - (hz * np.sin(i * var)) #demi cercle de rayon hz en z
        robot.jambe1(x, y, bz)
        robot.jambe5(x, y, bz)
        y = (ty / 2) - i
        robot.jambe3(x, y, bz)

        # deplacment triangle B
        y = -(ty / 2) + i
        bx = (x + correction1) * np.sin(20 - (i*20))
        robot.jambe2(x, y, z)
        robot.jambe4(x, y, z)
        y = (ty / 2) - i
        bx = (x + correction2) * np.sin(i * var)
        robot.jambe6(x, y, z)
        time.sleep(0.005)


def attack():
    #difinition des constantes
    bz = 50
    #z = 140
    dx, dy, dz = 0, 102, -110
    
    for i in range(0,bz):
        n_z = z - i
        #point fixe pour les jambes 1 et 2
        robot.jambe1(dx, dy, dz)
        robot.jambe2(dx, dy, dz)
        #haut bas pour les autres jambes
        x, y = 100, -100
        robot.jambe3(x, y, n_z)
        robot.jambe6(x, y, n_z)
        x, y = 100, 100
        robot.jambe4(x, y, 45)
        robot.jambe5(x, y, 45)
        time.sleep(0.05)

    for i in range(0,bz):
        n_z = (z - bz) + i
        #point fixe pour les jambes 1 et 2
        robot.jambe1(dx, dy, dz)
        robot.jambe2(dx, dy, dz)
        #haut bas pour les autres jambes
        x, y = 100, -100
        robot.jambe3(x, y, n_z)
        robot.jambe6(x, y, n_z)
        x, y = 100, 100
        robot.jambe4(x, y, 45)
        robot.jambe5(x, y, 45)
        time.sleep(0.05)

def move_boby(phi, r):
    phi = m.radians(phi)
    #z = 100
    y = r * np.sin(phi)
    x = 150 + (r * np.cos(phi))
    robot.jambe1(x, y, z)
    robot.jambe6(x, y, z)
    robot.jambe5(x, y, z)

    y = r * np.sin(phi)
    x = 150 - (r * np.cos(phi))
    robot.jambe2(x, -y, z)
    robot.jambe3(x, -y, z)
    robot.jambe4(x, -y, z)