# /usr/bin/python3

import pypot.dynamixel


def jambe1(theta1, theta2, theta3, dxl_io):
    # penser a integrer le decalage specifique a la jambe.

    dxl_io.set_goal_position({11: theta1, 12: theta2, 13: theta3})


def jambe2(theta1, theta2, theta3, dxl_io):
    dxl_io.set_goal_position({21: theta1, 22: theta2, 23: theta3})


def jambe3(theta1, theta2, theta3, dxl_io):
    dxl_io.set_goal_position({31: theta1, 32: theta2, 33: theta3})


def jambe4(theta1, theta2, theta3, dxl_io):
    # penser a integrer le decalage specifique a la jambe.
    dxl_io.set_goal_position({41: theta1, 42: theta2, 43: theta3})


def jambe5(theta1, theta2, theta3, dxl_io):
    #  dxl_io.set_goal_position({51: theta1, 52: theta2, 53: theta3})
    dxl_io.set_goal_position({52: theta2, 53: theta3})


def jambe6(theta1, theta2, theta3, dxl_io):
    dxl_io.set_goal_position({61: theta1, 62: theta2, 63: theta3})


# definition des triangles pour le deplacement
def triangle_A(theta1, theta2, theta3, dxl_io):
    jambe1(theta1, theta2, theta3, dxl_io)
    jambe3(theta1, theta2, theta3, dxl_io)
    jambe5(theta1, theta2, theta3, dxl_io)


def triangle_B(theta1, theta2, theta3, dxl_io):
    jambe2(theta1, theta2, theta3, dxl_io)
    jambe4(theta1, theta2, theta3, dxl_io)
    jambe6(theta1, theta2, theta3, dxl_io)


def all_m(theta1, theta2, theta3, dxl_io):
    triangle_A(theta1, theta2, theta3, dxl_io)
    triangle_B(theta1, theta2, theta3, dxl_io)
