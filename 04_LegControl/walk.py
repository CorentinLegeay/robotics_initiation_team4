#!/usr/bin/python3
import pypot
import time
import function
import numpy as np
import math
import structure_moteur


def walk_avant(x, z, ty, hz, dxl_io):
    while 1:
        var = math.radians(180 / ty)
        # deplacement sur x de ty a -ty
        for i in range(0, ty):
            # deplacement triangle A
            y = (ty / 2) - i
            bz = z - (hz * np.sin(i * var))
            theta1, theta2, theta3 = function.leg_ik(x, y, bz)
            structure_moteur.triangle_A(theta1, theta2, theta3, dxl_io)
            print("x = {}, y = {}, z = {}".format(round(x, 3), round(y, 3), round(bz, 3)))
            # deplacment triangle B
            y = -(ty / 2) + i
            theta1, theta2, theta3 = function.leg_ik(x, y, z)
            structure_moteur.triangle_B(theta1, theta2, theta3, dxl_io)
            print("x = {}, y = {}, z = {}".format(round(x, 3), round(y, 3), round(z, 3)))
            time.sleep(0.005)

        # deplacement en x de -ty a ty
        for i in range(0, ty):
            # deplacement triangle A
            y = -(ty / 2) + i
            theta1, theta2, theta3 = function.leg_ik(x, y, z)
            structure_moteur.triangle_A(theta1, theta2, theta3, dxl_io)
            print("x = {}, y = {}, z = {}".format(round(x, 3), round(y, 3), round(z, 3)))
            # deplacment triangle B
            y = (ty / 2) - i
            bz = z - (hz * np.sin(i * var))
            theta1, theta2, theta3 = function.leg_ik(x, y, bz)
            structure_moteur.triangle_B(theta1, theta2, theta3, dxl_io)
            print("x = {}, y = {}, z = {}".format(round(x, 3), round(y, 3), round(bz, 3)))
            time.sleep(0.005)


def walk_arriere(x, z, ty, hz, dxl_io):
    while 1:
        var = math.radians(180 / ty)
        # deplacement en x de ty a -ty
        for i in range(0, ty):
            # deplacement triangle A
            y = (ty / 2) - i
            theta1, theta2, theta3 = function.leg_ik(x, y, z)
            structure_moteur.triangle_A(theta1, theta2, theta3, dxl_io)
            print("x = {}, y = {}, z = {}".format(round(x, 3), round(y, 3), round(z, 3)))
            # deplacement triangle B
            y = -(ty / 2) + i
            bz = z - (hz * np.sin(i * var))
            theta1, theta2, theta3 = function.leg_ik(x, y, bz)
            structure_moteur.triangle_B(theta1, theta2, theta3, dxl_io)
            print("x = {}, y = {}, z = {}".format(round(x, 3), round(y, 3), round(bz, 3)))
            time.sleep(0.005)

        for i in range(0, ty):
            y = -(ty / 2) + i
            bz = z - (hz * np.sin(i * var))
            theta1, theta2, theta3 = function.leg_ik(x, y, bz)
            structure_moteur.triangle_A(theta1, theta2, theta3, dxl_io)
            print("x = {}, y = {}, z = {}".format(round(x, 3), round(y, 3), round(bz, 3)))
            # deplacement triangle B
            y = (ty / 2) - i
            theta1, theta2, theta3 = function.leg_ik(x, y, z)
            structure_moteur.triangle_B(theta1, theta2, theta3, dxl_io)
            print("x = {}, y = {}, z = {}".format(round(x, 3), round(y, 3), round(z, 3)))
            time.sleep(0.005)
