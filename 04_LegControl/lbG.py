# /usr/bin/python3

#  importation des librairies
import pypot.dynamixel
import time
from pypot import *
import io
import function
import numpy as np
import math
import walk

try:
    #   programme principal
    if __name__ == '__main__':

        ports = pypot.dynamixel.get_available_ports()

        if not ports:
            raise IOError('no port found!')

        print('ports found', ports)

        print('connecting on the first available port:', ports[0])
        dxl_io = pypot.dynamixel.DxlIO(ports[0])

        # initialisation des moteurs:
        dxl_io.set_joint_mode({11, 12, 13})
        dxl_io.set_goal_position({11: 0, 12: 0, 13: 0})
        dxl_io.set_angle_limit({11: (-100, 100), 12: (-100, 100), 13: (-110, 70)})

        time.sleep(0.2)

        # choix du mode de fonctionnement:
        print("mode de fonctionnement:")
        print("direct = 1, inverse = 2, cercle = 3, all = 4, marche = 5")
        mode = int(input("mode = "))
        print("")

        if mode == 1:
            print("mode cinematique direct")
            while 1:
                # passage en mode manuel des servo moteur
                dxl_io.disable_torque({11, 12, 13})

                # on recupere la position des 3 servos moteurs
                thetas = dxl_io.get_present_position({11, 12, 13})
                print("Theta 1 = {}, Theta 2 = {}, Theta 3 ={}".format(thetas[0], thetas[1], thetas[2]))

                # envoie vers la fonction et recuperation des valeurs :
                x, y, z = function.leg_dk(thetas[0], thetas[1], thetas[2])
                print("x = {}, y = {}, z = {}".format(round(x, 3), round(y, 3), round(z, 3)))
                print("")
                time.sleep(0.02)

        if mode == 2:
            print("mode cinematique inverse")
            print("entrée les valeurs des coordonnée")
            x = float(input("x = "))
            y = float(input("y = "))
            z = float(input("z = "))

            # envoie des valeur dans la fonction
            theta1, theta2, theta3 = function.leg_ik(x, y, z)
            print(
                "theta 1 = {}, theta 2 = {}, theta 3 = {}".format(round(theta1, 3), round(theta2, 3), round(theta3, 3)))
            print("")
            dxl_io.set_goal_position({11: theta1, 12: theta2, 13: theta3})

        if mode == 3:
            print("mode cercle")
            print("cercle fixe en z = 1, fixe en x = 2")
            mode2 = int(input("mode = "))
            taille_cercle = int(input("taille du cercle = "))
            '''phi = 0
            r = 50
            while 1:
                for phi in range(0,360):
                    phi = math.radians(phi)
                    z = r*np.sin(phi)
                    y = r*np.cos(phi)
                    x = 255
                    print(x,y,z)
                    theta1,theta2,theta3 = function.leg_ik(x,y,z)
                    dxl_io.set_goal_position({11: theta1, 12: theta2, 13: theta3})
                    time.sleep(0.01)'''
            while 1:
                for i in range(0, 360):
                    x, y, z = function.cercle(i, taille_cercle, mode2)
                    theta1, theta2, theta3 = function.leg_ik(x, y, z)
                    dxl_io.set_goal_position({11: theta1, 12: theta2, 13: theta3})
                    time.sleep(0.01)

        if mode == 4:
            print("mode cinematique directet et indirecte")
            while 1:
                # passage en mode manuel des servo moteur
                dxl_io.disable_torque({11, 12, 13})

                # on recupere angles des 3 servos moteurs
                thetas = dxl_io.get_present_position({11, 12, 13})
                print("Angles mote: Theta 1 = {}, Theta 2 = {}, Theta 3 ={}".format(round(thetas[0], 3),
                                                                                    round(thetas[1], 3),
                                                                                    round(thetas[2], 3)))

                # envoie vers la fonction et recuperation des valeurs :
                x, y, z = function.leg_dk(thetas[0], thetas[1], thetas[2])

                # utilisation de la cinematique inverse
                thetas = function.leg_ik(x, y, z)
                print("angles réel: Theta 1 = {}, Theta 2 = {}, Theta 3 ={}\n".format(round(thetas[0], 3),
                                                                                      round(thetas[1], 3),
                                                                                      round(thetas[2], 3)))
                print("x = {}, y = {}, z = {}".format(round(x, 3), round(y, 3), round(z, 3)))

                print("")
                time.sleep(0.02)

        if mode == 5:
            print("mode marche")
            x = 150
            z = -70
            ty = 100  # deplacement en mm
            hz = 30  # hauteur en mm
            walk.walk_avant(x, z, ty, hz, dxl_io)

except KeyboardInterrupt:  # permet l'interruption avec CTRL+C
    # incrire le programme à effectuer en cas d'exception
    print("\nVous avez interrompue l'execution")

finally:
    # partie final à executer pour terminer le programme
    dxl_io.disable_torque({11, 12, 13})  # si l'erreur n'est pas résolu stopper la force des moteurs
    print("-------")
    print("finish wait 2 sec")
    time.sleep(2)
    SystemExit  # permet de sortir du programme
