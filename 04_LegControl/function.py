#!/usr/bin/python3
# coding : utf8

import pypot.dynamixel
import time
from pypot import *
import io
import numpy as np
import math

'''
alpha = 15
beta = 44
l1= 54.8
l2= 65.3
l3=133
'''
alpha = 16
beta = 43.76
l1 = 54.8
l2 = 65.3
l3 = 133


# fonction de correction des angles thetas
def correction_angles_direct(theta1, theta2, theta3):
    '''
    Correction des angles pour la cinématique directe
    '''
    # correction à appliquer
    theta2_c = alpha
    theta3_c = -theta2_c + beta

    # application de la correction sur les angles
    theta2 = theta2 - theta2_c
    theta3 = -(theta3 - theta3_c)

    return theta1, theta2, theta3


# fonction de correction des angles thetas
def correction_angles_inverse(theta1, theta2, theta3):
    '''
    Correction des angles pour la cinématique inverse
    '''
    # correction à appliquer
    theta2_c = -alpha
    theta3_c = theta2_c + beta

    # application de la correction sur les angles
    theta2 = theta2 - theta2_c
    theta3 = -(theta3 - theta3_c)

    return theta1, theta2, theta3


def leg_dk(theta1, theta2, theta3):
    '''
    Fonction de cinématique directe
    '''
    theta1, theta2, theta3 = correction_angles_direct(theta1, theta2, theta3)
    print("Angles corr: Theta 1 = {}, Theta 2 = {}, Theta 3 = {}".format(round(theta1, 3), round(theta2, 3),
                                                                         round(theta3, 3)))
    theta1 = math.radians(theta1)
    theta2 = math.radians(theta2)
    theta3 = math.radians(theta3)

    D12 = l2 * (math.cos(theta2))
    D23 = l3 * (math.cos(theta2 - theta3)) # -

    x = (l1 + D12 + D23) * math.cos(theta1)
    y = (l1 + D12 + D23) * (math.sin(theta1))
    z = (l2 * (math.sin(theta2))) + (l3 * (math.sin(theta2 - theta3))) # -

    return x, -y, -z


#  Correction pour la cinématique inverse
def alkashi(a, b, c):
    acosx = math.pow(a, 2) + math.pow(b, 2) - (math.pow(c, 2))
    acosy = 2 * a * b
    value = acosx / acosy
    if value >= 1:
        value = 1
        print('Attention ! (alkashi)')
    if value <= -1:
        value = -1
        print('Attention ! (alkashi)')
    acos = math.acos(value)
    return acos


# fonction leg_ik
def leg_ik(x3, y3, z3):
    y3, z3 = -y3, -z3
    # Calcul de l'angle Theta1 :
    theta1 = math.atan2(y3, x3)
    # Calcul de l'angle Theta 2 :
    dproj = math.sqrt(math.pow(x3, 2) + math.pow(y3, 2))  # on calcul la distance d, projetee sur le plan (X,Y)
    d13 = dproj - l1  # on calcul la distance d13, sur le plan (X,Y)

    if d13 < 0:
        print("Destination point too close")
        d13 = 0

    d = math.sqrt((math.pow(d13, 2) + math.pow(z3, 2)))
    if d > l2 + l3:
        print("Destination point too far away")
        d = l2 + l3

    a = math.atan2(z3, d13)  #  on calcul l'angle a (theta2 = a + b)
    b = alkashi(l2, d, l3)  #  on calcul l'angle b (theta2 = a + b)
    theta2 = a + b
    # Calcul de l'angle Theta 3 :
    theta3 = -alkashi(l2, l3, d) + math.pi
    # print("Cinematique inverse theta3", theta3)

    # Conversion en degres des angles
    theta1 = math.degrees(theta1)
    theta2 = math.degrees(theta2)
    theta3 = math.degrees(theta3)

    #  print("Angles sans correction leg_ik : ", theta1, theta2, theta3)
    theta1, theta2, theta3 = correction_angles_inverse(theta1, theta2, theta3)

    theta2 = modulo180(theta2)
    theta3 = modulo180(theta3)
    #  print("Angles fin de programme leg_ik : ", theta1, theta2, theta3)

    return theta1, theta2, theta3


# Takes an angle that's between 0 and 360 and returns an angle that is between -180 and 180
def modulo180(angle):
    if -180 < angle < 180:
        return angle

    angle = angle % 360
    if angle > 180:
        return -360 + angle

    return angle


def cercle(phi, r, plan):
    phi = math.radians(phi)
    if plan == 1:  # plan x,y
        z = 100
        y = r * np.sin(phi)
        x = 150 + r * np.cos(phi)
        return x, y, z
    if plan == 2:  # plan y,z
        z = r * np.sin(phi)
        y = r * np.cos(phi)
        x = 240
        return x, y, z
