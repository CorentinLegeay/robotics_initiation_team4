#!/usr/bin/python3
# coding : utf8
#  PROGRAMME CINEMATIQUE INVERSE

# importation des librairies :
import math as m
import numpy as np
import pypot


# fonction de correction des angles thetas
def correction_angles(theta1, theta2, theta3):
    # correction à appliquer
    theta2_c = -20.69
    theta3_c = 90 + theta2_c - 5.06

    # application de la correction sur les angles
    theta2 = theta2 - theta2_c
    theta3 = -(theta3 - theta3_c)

    return theta1, theta2, theta3


# fonction AlKashi
def alkashi(a, b, c):
    acos = m.acos((m.pow(a, 2) + m.pow(b, 2) - (m.pow(c, 2))) / (2 * a * b))
    return acos


# fonction leg_ik
def leg_ik(x3, y3, z3, l1, l2, l3):
    # Calcul de l'angle Theta1 :
    theta1 = m.atan2(y3, x3)
    # Calcul de l'angle Theta 2 :
    dproj = m.sqrt(m.pow(x3, 2) + m.pow(y3, 2))  # on calcul la distance d, projetee sur le plan (X,Y)
    d13 = dproj - l1  # on calcul la distance d13, sur le plan (X,Y)
    d = m.sqrt((m.pow(d13, 2) + m.pow(z3, 2)))
    a = m.atan2(z3, d13)  #  on calcul l'angle a (theta2 = a + b)
    b = alkashi(l2, d, l3)  #  on calcul l'angle b (theta2 = a + b)
    theta2 = a + b
    # Calcul de l'angle Theta 3 :
    theta3 = -alkashi(l2, l3, d) + m.pi
    theta1 = m.degrees(theta1)
    theta2 = m.degrees(theta2)
    theta3 = m.degrees(theta3)

    return theta1, theta2, theta3


def main():

    #  Tableaux contenant les coordonnées et les angles :
    tab_coordonnees = [
        [np.sin(0), np.sin(0), np.sin(0)],
        [0, 118.79, -115.14],
        [-64.14, 0, -67.19],
        [203.23, 0, -14.30],
    ]
    tab_angle = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
    ]

    # Calcul des angles :
    for i in range(0, 4):
        Px3, Py3, Pz3 = tab_coordonnees[i]  #  on récupère les coordonnées
        Theta1, Theta2, Theta3 = leg_ik(Px3, Py3, Pz3, 51, 63.7, 93)  # on calcul les angles (sans correction)

        Theta1, Theta2, Theta3 = correction_angles(Theta1, Theta2, Theta3)  # on applique la correction

        # on arrondi le résultat pour une meilleur visibilité lors de l'affichage
        Theta1 = round(Theta1, 2)
        Theta2 = round(Theta2, 2)
        Theta3 = round(Theta3, 2)
        # on stock la valeur des angles dans le tableau tab_angle
        tab_angle[i] = Theta1, Theta2, Theta3
        # on affiche le résultat
        print("Coordonnées de P3 :", tab_coordonnees[i], "-> Angles :", tab_angle[i])


main()
