#!/usr/bin/python3

import math
from tkinter import *

def affichage(tab):
    #affichage de tous les resultat
    fenetre = Tk()
    fenetre.title('valeur')
    for i in range(len(tab[0])):
        tableau=Label(fenetre, text=tab[0][i])
        tableau.grid(row=0, column=i)
        tableau=Label(fenetre, text=tab[1][i])
        tableau.grid(row=1, column=i)
        tableau=Label(fenetre, text=tab[2][i])
        tableau.grid(row=2, column=i)
    quitte=Button(fenetre,text='Quitter',command=fenetre.destroy)
    quitte.grid(row=3,column=0)
    fenetre.mainloop()

def Alkashi (a,b,c):
    alkashi = math.acos((a**2+b**2-c**2)/(2*a*b))
    return alkashi
    
def leg_DK(theta1,theta2,theta3):
    #application de la correction d'angle:
    theta2_c=-20.69
    theta3_c=90 + theta2_c - 5.06

    theta2 = (theta2 - theta2_c)
    theta3 = (theta3_c - theta3_c)
    
    #calcule de la position de P3 en fonction des Thetas
    D12 = L2*(math.cos(theta2))
    D23 = L3*(math.cos(theta2+theta3))

    x=(L1+D12+D23)*math.cos(theta1)
    y=(L1+D12+D23)*math.sin(theta1)
    z=(L2*math.sin(theta2))+(L3*math.sin(theta2+theta3))
    return x,y,z


def leg_ik(x,y,z):
    #calcule des thetas en fonction de la position de P3
    d13 = ((math.sqrt(y**2+x**2))-L1)
    l3p = (math.sqrt(z**2+d13**2))
    a = (math.atan(z/d13))
    b=Alkashi(L2,l3p,L3)
    c=Alkashi(L2,L3,l3p)

    if x == 0:
        theta1 = math.radians(90)
    else :
        theta1 = math.atan(y/x)
    theta2 = a+b

    theta3 = c+(math.pi)
    
    #application des correction d'angles
    theta2_c=math.radians(-20.69)
    theta3_c=math.radians(90 + theta2_c - 5.06)

    theta2 = (theta2_c + theta2)
    theta3 = (theta3 - theta3_c)
    
    #envoie pour validation des calcule en kimatic direct
    xd,yd,zd = leg_DK(theta1,theta2,theta3)

    #envoie pour affichage de toutes les valeur
    tab = [
    ["theta IK (°): ",round(math.degrees(theta1),3),round(math.degrees(theta2),3),round(math.degrees(theta3),3)],
    ["valeur initial (mm): ",round(x*1000,3),round(y*1000,3),round(z*1000,3)],
    ["valeur apres validation (mm): ",round(xd*1000,3),round(yd*1000,3),round(zd*1000,3)]]
    affichage(tab)

#======================================================
#definition des constantes (en m)
L1=51/1000
L2=63.7/1000
L3=93/1000

#Blabla 
print("longeur predefinie:")
print("longeur 1 = {0}m, longeur 2 = {1}m, longeur 3 = {2}m".format(L1,L2,L3))
print("")

#entrée des valeur de P3
print("entrée les valeurs de P3:")
'''
x=(float(input("x =")))
y=(float(input("y =")))
z=(float(input("z =")))
'''
x,y,z=0,118.79,-115.14
#conversion mm en m 
x = x/1000
y = y/1000
z = z/1000
print("")

leg_ik(x,y,z)
