#!/usr/bin/python3

#import de la librairie math permettant de réaliser les calcul
import math as m

#définition de la fonction principal contenant l'ensemble des résultats
def leg_ik(x3,y3,z3,l1,l2,l3) :
    #calcul des distances
    dp = m.sqrt((x3**2) + (y3**2))
    d13 = dp - l1
    d = m.sqrt(m.pow(d13,2) + m.pow(z3,2))
    #calcul des angles
    a = m.atan(z3/d13)
    b = alkashi(l2,d,l3)
    c = alkashi(l2,l3,d)
    #calcul des theta
    theta1 = m.atan(y3/x3)
    theta2 = a + b
    theta3 = -c + (m.pi)

    #conversion radian en degre
    theta1 = m.degrees(theta1)
    theta2 = m.degrees(theta2)
    theta3 = m.degrees(theta3)

    print("angle non corrigés = theta 1 : {}, theta2 : {}, theta3 : {}".format(theta1,theta2,theta3))
    correction_angle(theta1,theta2,theta3)

    print("angles finaux corrigés = theta 1 : {}, theta 2 : {}, theta 3 : {}".format(theta1,theta2,theta3))
    

#définition de la fonction permettant d'utiliser la formule d'AL Kashi
def alkashi(a,b,c) :
    alkashi = m.acos((a**2 + b**2 - c**2)/(2*a*b))
    return alkashi

#définition de la fonction permettant de réaliser les corrections d'angles
def correction_angle(theta1,theta2,theta3) :
    t2c = -20.69
    t3c = 90 + t2c - 5.06
    #application correction
    theta2 = theta2 - t2c
    theta3 = -(theta3 - t3c)
    return theta1,theta2,theta3

#définition de la fonction d'initialisation des valeurs de P3
def main() : 
    print("Rentrer la position de P3 : ")
    print("----------------------------")

    x3 = int(input("Px3 = "))
    y3 = int(input("Py3 = "))
    z3 = int(input("Pz3 = "))

    l1 = 51
    l2 = 63.7
    l3 = 93

    leg_ik(x3,y3,z3,l1,l2,l3)
    
#appel de la première fonction
main()